package com.test.finalpracticeapp.TooltipLayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.test.finalpracticeapp.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var tooltip: Tooltip? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val metrics = resources.displayMetrics

        button1.setOnClickListener { button ->

            val gravity = Tooltip.Gravity.valueOf("TOP")
            val closePolicy = getClosePolicy()
            //val typeface = if (checkbox_font.isChecked) Typefaces[this, "fonts/GillSans.ttc"] else null
            val arrow = true
            val overlay = true
            //val style = if (checkbox_style.isChecked) R.style.ToolTipAltStyle else null
            val text = "Hello Ankush"
                //if (text_tooltip.text.isNullOrEmpty()) text_tooltip.hint else text_tooltip.text!!.toString()


            tooltip?.dismiss()

            tooltip = Tooltip.Builder(this)
                .anchor(button, 0, 0, false)
                .text(text)
                .styleId(null)
                //.typeface(typeface)
                .maxWidth(metrics.widthPixels / 2)
                .arrow(arrow)
                .floatingAnimation(Tooltip.Animation.DEFAULT)
                .closePolicy(closePolicy)
                .showDuration(3000)
                .overlay(overlay)
                .create()

            tooltip
                ?.doOnHidden {
                    tooltip = null
                }
                ?.doOnFailure { }
                ?.doOnShown {}
                ?.show(button, gravity, true)
        }

        /*button2.setOnClickListener {
            val fragment = TestDialogFragment.newInstance()
            fragment.showNow(supportFragmentManager, "test_dialog_fragment")
        }

        seekbar_duration.doOnProgressChanged { numberPicker, progress, formUser ->
            text_duration.text = "Duration: ${progress}ms"
        }*/
    }

    private fun getClosePolicy(): ClosePolicy {
        val builder = ClosePolicy.Builder()
        builder.inside(true)
        builder.outside(true)
        builder.consume(true)
        return builder.build()
    }

    override fun onDestroy() {
        super.onDestroy()
        tooltip?.dismiss()
    }
}
