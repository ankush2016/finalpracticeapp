package com.test.finalpracticeapp.MyWorkManager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.test.finalpracticeapp.R
import com.test.testmodule1.FirstActivity
import com.test.testmodule1.HelloMe
import kotlinx.android.synthetic.main.activity_my_constraint_layout.*
import java.util.*

class WorkManagerMainActivity : AppCompatActivity() {

    lateinit var tvWorkStatus: TextView

    companion object {
        val KEY_TASK_DESC = "key_task_desc"
        val KEY_TASK_OUTPUT = "key_task_output"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_manager_main)

        //startActivity(Intent(this, FirstActivity::class.java))
        tvWorkStatus = findViewById(R.id.tvWorkStatus)

        val data = Data.Builder()
                .putString(KEY_TASK_DESC, "Hey I am sending the work data")
                .build()

        var request = OneTimeWorkRequest.Builder(MyWorker::class.java).setInputData(data).build()

        findViewById<Button>(R.id.bOneTimeWork).setOnClickListener {
            WorkManager.getInstance(this@WorkManagerMainActivity).enqueue(request)
        }

        WorkManager.getInstance(this).getWorkInfoByIdLiveData(request.id)
                .observe(this, object : Observer<WorkInfo> {
                    override fun onChanged(workInfo: WorkInfo?) {
                        if (workInfo?.state?.isFinished!!) {
                            val outputData = workInfo?.outputData
                            val output = outputData.getString(KEY_TASK_OUTPUT)
                            tvWorkStatus.append(output + "\n")
                        }
                        var status = workInfo?.state?.name
                        tvWorkStatus.append(status + "\n")
                    }
                })
    }
}
