package com.test.finalpracticeapp.MyWorkManager

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.test.finalpracticeapp.R

class MyWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {


    override fun doWork(): Result {
        val data = inputData
        displayNotification("I am your work", data.getString(WorkManagerMainActivity.KEY_TASK_DESC))
        val outData = Data.Builder().putString(WorkManagerMainActivity.KEY_TASK_OUTPUT, "Hi Ankush - Task finished successfully").build()
        return Result.success(outData)
    }

    private fun displayNotification(task: String?, desc: String?) {
        var manager: NotificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var channel = NotificationChannel("ankushkapoor", "ankushkapoor", NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        var builder: NotificationCompat.Builder = NotificationCompat.Builder(applicationContext, "ankushkapoor")
                .setContentTitle(task)
                .setContentText(desc)
                .setSmallIcon(R.drawable.download)
        manager.notify(1, builder.build())
    }
}