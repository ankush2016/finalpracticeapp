package com.test.finalpracticeapp.Extras

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.test.finalpracticeapp.R
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        var url = "https://www.netflix.com/in/"
        var webSettings = webviewTest.settings
        webSettings.javaScriptEnabled = true
        webviewTest.loadUrl(url)
    }
}
