package com.test.finalpracticeapp.Extras

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebViewClient
import com.test.finalpracticeapp.R
import kotlinx.android.synthetic.main.activity_web_view_test.*

class WebViewTest : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view_test)

        //webView.loadUrl("http://m.durianfm.com/")
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = WebViewClient()
        webView.loadUrl("http://m.durianfm.com/")
        //webView.loadUrl("https://www.google.com/")
    }
}
