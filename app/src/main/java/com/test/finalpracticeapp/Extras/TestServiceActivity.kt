package com.test.finalpracticeapp.Extras

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.test.finalpracticeapp.R

class TestServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_service)
    }
}
