package com.test.finalpracticeapp.Extras

fun main() {
    print("Hi")
}

open class Animal {
    var color = "red"
}

class KotlinTest : Animal() {

}

class Swift : Car() {
    override lateinit var color: String
    override fun getPrice() {
    }

    override fun getLength() {
    }

}

abstract class Car {
    open lateinit var color: String

    abstract fun getPrice()

    open fun getLength() {}

    fun getHeight() {
    }
}