package com.ankush.myfirstlibrary

data class Player(val name: String, val age: Int)